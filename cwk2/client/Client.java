import java.net.*;
import java.io.*;
import java.util.*;


/*
 * This class represents a client during client/server communication. When
 * it is run, with the correct command line arguements, it will either request
 * the server to increase the votes for the given options in the server's poll,
 * or it will request the state of the poll from the server and display it.
 */
public class Client {

	// Initialising the socket for for the server.
	private Socket server = null;

	// Initialising the clients output writer to server.
	private PrintWriter clientOut = null;

	// Initialising the cliens input reader from server.
	private BufferedReader clientIn = null;

	// Initialising the string to print data back to the client terminal.
	private String pollPrint = null;

	// Initialising the variable to store the number of arguements used
	private int al = 0;


	/*
	 *This method will call the functions required to run the client.
	 */
	public void runClient(String[] args) {
		connectClient();

		clientWrite(args);

		clientRead();

		closeClient();
	}


	/*
	 * This method will try to connect the client to the server and create an
	 * output and input stream between the client and server.
	 */
	public void connectClient() {
		try {
			// Try and create the socket.
			server = new Socket("localhost", 7777);

			// Chain a writing stream.
			clientOut = new PrintWriter( server.getOutputStream(), true );

			// Chain a reading stream.
			clientIn = new BufferedReader(
							new InputStreamReader( server.getInputStream()) );
		}
		catch( UnknownHostException e ) {
			System.err.println( "Don't know about host.\n" );
			System.exit(1);
		}
		catch( IOException e ) {
			System.err.println( "Couldn't get I/O for the connection to host.\n" );
			System.exit(1);
		}
	}


	/*
	 * This method writes from the client to the server. After all arguements
	 * given to the client via the command line are written to the server,
	 * the method then writes 'end', to signify that all arguements have been
	 * passed.
	 */
	public void clientWrite( String[] args ) {
		// Set the number of arguements.
		al = args.length;

		if(args[0].equalsIgnoreCase("show")) {
			clientOut.println(args[0]);
			clientOut.println("end");
		}
		else if(args[0].equalsIgnoreCase("vote")) {

			for(int i=0; i<al; i++) {
				clientOut.println(args[i]);
			}
			clientOut.println("end");
		}
	}


	/*
	 * This method reads from the server to the client. It will continue to read
	 * until the server sends 'end' to signify that it has passed everything it
	 * needs to. Once it recieves 'end', it will print the content recieved to
	 * the client terminal.
	 */
	public void clientRead() {
		try {
			while((pollPrint = clientIn.readLine()) != null) {
				if(!pollPrint.equals("end")) {
					System.out.println(pollPrint);
				}
				else {
					break;
				}
			}
		}
		catch (IOException e) {
			System.err.println( e );
		}
	}


	/*
	 * This method tries to close the client read and client write stream, before
	 * closing the connection to the server.
	*/
	public void closeClient() {
		try {
			clientOut.close();
			clientIn.close();
			server.close();
		}
		catch (IOException e) {
			System.err.println("I/O exception during execution\n");
			System.exit(1);
		}
	}


	/*
	 * The main method  makes sure the first arguement passed from the
	 * command line is either show or vote. If it is, then a new client object
	 * is created and runClient is called.
	 */
	public static void main( String[] args ) {
		if(args[0].equalsIgnoreCase("show") || args[0].equalsIgnoreCase("vote")) {
			Client c = new Client();
			c.runClient(args);
		}
		else {
			System.err.println("Arguement 1 must be either 'show' or 'vote'.");
		}
	}
}
