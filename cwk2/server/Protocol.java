import java.net.*;
import java.io.*;


/*
 * This class represents the protocol taken with client/server communication.
 * It determines how the server responds to client requests.
 */
public class Protocol {
  // Initialising the 'REQUEST' and 'VOTE' states for the protocol.
  private static final int REQUEST = 0;
  private static final int VOTE = 1;

  // Initialising the starting state to 'REQUEST'.
  private int state = REQUEST;

  // Initialising the poll and poll length (number of poll options) to null.
  private Poll p[] = null;
  private int pl = 0;

  // Initialising the server input and output to null.
  private String input = null, output = null;


  /*
   * This constructor initialises the protocol object, assigning variable p,
   * the current poll state and pl, the amount of options in the poll.
   */
  public Protocol(Poll[] p) {
    this.p = p;
    this.pl = p.length;
  }


  /*
   * This method processes the client request and outputs a response to the
   * request.
   */
  public String processInput(String input) {
    input = input;
    output = null;

    if(state == REQUEST) {
      output = request(input);
    }
    else if(state == VOTE) {
      output = vote(input);
    }
    // The only thing that could have happened here, is that the option selected
    // to be voted for, was not an option in the poll.
    if(output == null) {
      output = (input + " is not an option in the poll.\n");
      output += "end";
    }

    return output;
  }


  /*
   * This method handles the first arguement of the request, which is either
   * 'show' or 'vote'. If it is show, then the method returns the current state
   * of the poll to be returned to the client. If it was 'vote', then the
   * protocol moves on to the 'VOTE' state.
   */
  public String request(String input) {
    if(input.equalsIgnoreCase("show")) {
      output = ("'" + p[0].getChoice() + "' has " + p[0].getCount() + " vote(s).\n");
      for(int i=1; i<pl; i++) {
        output += ("'" + p[i].getChoice() + "' has " + p[i].getCount() + " vote(s).\n");
      }
      output += "end";
    }
    else if(input.equalsIgnoreCase("vote")) {
      state = VOTE;
      output = "vote";
    }

    return output;
  }


  /*
   * This method increases the count of the option in the poll that has been
   * voted for by the client. It then returns the new amount of votes for that
   * option, to be returned to the client.
   */
  public String vote(String input) {
    for(int i=0; i<pl; i++) {
      if(input.equalsIgnoreCase(p[i].getChoice())) {
        p[i].increaseCount();
        output = ("'" + p[i].getChoice() + "' now has " + p[i].getCount() + " vote(s).");
      }
      else if(input.equalsIgnoreCase("end")) {
        output = input;
      }
    }
    return output;
  }

}
