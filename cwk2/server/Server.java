import java.net.*;
import java.io.*;
import java.util.concurrent.*;


/*
 * This class represents the server which will handle upto 20 client requests
 * at a time. It will take a minimum of 2 command line arguements, which will
 * be used as the options in the poll. From here, further communication between
 * the server and client is delegated to the client handler and protocol to
 * ensure that client requests are satisfied.
 */
public class Server {

	// Initialising the server socket that clients will connect to.
	private ServerSocket server = null;

	// Initiating the executor service to handle threads and clients.
	private ExecutorService service = null;

	// Initialising the value for the number of command line arguements to 0.
	private int al = 0;

	// Initialising the poll.
	private Poll p[] = null;

	// Initialising the log file.
	private File log = null;


	/*
	 * This constructor initialises the server, initialising the 'p' array's
	 * choice variable to match the command line arguements given to the server.
	 * It then creates a server socket for 20 clients to connect to and creates
	 * the log file where log data will be stored.
	 */
	public Server(String[] args) {
		try {

			al = args.length;
	    p = new Poll[al];

			for(int i=0; i<al; i++) {
				p[i] = new Poll();
	      p[i].setChoice(args[i]);
	    }

			server = new ServerSocket(7777);

			File log = new File("log.txt");
			log.createNewFile();
		}
		catch (IOException e) {
				System.err.println("Could not listen on port: 7777.");
				System.exit(1);
		}
		// Initialise the executor with a fixed size of 20.
		service = Executors.newFixedThreadPool(20);
	}


	/*
	 * This method runs the server, accepting clients automatically and assigning
	 * them to the client handler, when there is space, until the server is shut
	 * down.
	 */
	public void runServer() {

		while( true ) {

			// For each new client, submit a new handler to the thread pool.
				try {
						Socket client = server.accept();
						service.submit( new ClientHandler(client, p) );
				}
				catch (IOException e) {
						System.err.println("Accept failed.");
						System.exit(1);
				}
		}
	}


	/*
	 * This method ensures that there were a minimum of 2 command line arguements
	 * given to the server, before creating a server and then running it.
	 */
	public static void main( String[] args ) {

		if(args.length<2) {
			System.err.println("Must have a minimum of two arguements given to the server.");
			System.exit(1);
		}
		else {
			Server s = new Server(args);
			s.runServer();
		}
	}
}
