import java.net.*;
import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;


/*
 * This class represents the client handler. When it is run, it handles the
 * servers interaction with the client and assists in making concurrent
 * server connections possible.
 */
public class ClientHandler extends Thread {
  // Initialising a socket that will have the client assigned to it.
  private Socket socket = null;

  // Initialising the read and write streams for the server.
  private PrintWriter serverOut = null;
  private BufferedReader serverIn = null;

  // Initialising an instance of the protocol class.
  private Protocol protocol = null;

  // Initialising a Poll variable to hold the current poll object.
  private Poll p[] = null;

  // Initialising a variable to store the input from the client and the output
  // to the client.
  private String inputLine = null, outputLine = null;


  /*
   * This constructor will accept a client socket and the current poll as
   * arguements and assign them to the ClientHandler object.
   */
  public ClientHandler(Socket socket, Poll[] p) {
    super("ClientHandler");
    this.socket = socket;
    this.p = p;
  }


  /*
   * This method will run the client handler and call the other required
   * methods. When it is finished, it will close the server side socket
   * connection to the client.
   */
  public void run() {
    try {
      // Initialising the input and output streams for the server.
      serverOut = new PrintWriter(socket.getOutputStream(), true);
      serverIn = new BufferedReader(
                     new InputStreamReader(socket.getInputStream()));

      // Initialising the protocol for this client/server connection.
      protocol = new Protocol(p);

      // Assigning the value recieved from the client to the input, before
      // determining an output with the use of the processInput method.
      inputLine = serverIn.readLine();
      outputLine = protocol.processInput(inputLine);

      writeLog(inputLine);

      serverWrite();



      // Closing the server input and output streams, followed by the client
      // socket connection.
      serverOut.close();
      serverIn.close();
      socket.close();
    }

    catch (IOException e) {
      System.err.println( e );
    }
  }


  /*
   * This method will write to the 'log.txt' file.
   */
  public void writeLog( String inputLine ) {
    try {
      // Getting the client IP address as a string value.
      InetAddress inet = socket.getInetAddress();
      String ip = inet.toString().replace("/","");

      // Formatting the date and time.
      String pattern = "dd-MM-yyyy : HH:mm:ss : ";
      SimpleDateFormat dtf = new SimpleDateFormat(pattern);
      String dateTime = dtf.format(new Date());

      // Initialising a write stream to the 'log.txt' file, then write to it,
      // before closing the stream.
      BufferedWriter logWrite = new BufferedWriter(new FileWriter("log.txt", true));
      logWrite.write(dateTime + ip + " : " + inputLine + "\n");
      logWrite.close();
    }
    catch(IOException e) {
      System.err.println( e );
			System.exit(1);
    }
  }


  /*
   * This method writes from the server to the client, providing the client
   * with the information that it has requested, so that it can print it to the
   * client terminal. It will continue to write until it has completed all
   * requests from the client.
   */
  public void serverWrite() {
    try {
      if(!outputLine.equalsIgnoreCase("vote")) {
        serverOut.println(outputLine);
      }

      while ((inputLine = serverIn.readLine()) != null) {
        outputLine = protocol.processInput(inputLine);
        serverOut.println(outputLine);
      }
    }
    catch( IOException e ) {
      System.err.println( e );
			System.exit(1);
    }
  }

}
