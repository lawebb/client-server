/*
 * This class represents the poll object that will be used by the server.
 */
public class Poll {
  // Initialising the 'choice' and 'count' variables of the poll.
  private String choice = null;
  private int count = 0;


  /*
   * This is a get method used to get the choice assigned to this poll option.
   */
  public String getChoice() {
    return this.choice;
  }


  /*
   * This is a get method used to get the number of votes assigned to this
   * poll option.
   */
  public int getCount() {
    return this.count;
  }


  /*
   * This is a set method used to set the choice of this poll option.
   */
  public void setChoice( String choice ) {
    this.choice = choice;
  }


  /*
   * This is a method to increment the vote count of the poll option by 1.
   */
  public void increaseCount() {
    this.count++;
  }
}
