# Command Line client-server poll

A server that can accept 20 clients at a time, to either vote for options on its
poll, or request to see the standings of the poll.


# Why I made this programm

I made this programm as a project during my second year of university. This
was my first time creating a proper server and working with server protocols.


# Features

    * A server can be created with a minimum of 4 command line arguments, that
        will be used as options in its poll.
    * The server can accept a pool of up to 20 clients at a time.
    * The client can choose to eithe vote for 1 or more options, or to show the
        current state of the poll (both via command line arguments)
    * Each request from a client is logged in a file called log.txt (which is
        created when the server is run).
    
    
# How to run the server on Linux

Set up directories as shown in my repository and cd into the server directory.

Server Terminal:

    * $ javac *.java
    * $ java Server <option> <option> <option>
    
    
# How to run the client on Linux

Set up directories as shown in my repository and cd into the client directory.

Client terminal:

    * $ javac *.java
    * $ java Client <vote or show> <option>
    

# Working Examples

The server running continuously and multiple clients voting:

![working](/images/server-client.png)


The output in log.txt:

![log](/images/log.png)


# Specification

[Link 1: Specification](https://gitlab.com/lawebb/client-server/-/blob/master/specification/coursework.pdf)


